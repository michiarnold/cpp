FROM ubuntu:19.04

# Configure apt
ENV DEBIAN_FRONTEND=noninteractive
RUN TZ=Europe/Berlin


RUN apt-get update
# Configure apt
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update \
    && apt-get -y install --no-install-recommends apt-utils 2>&1

# Install git, process tools, lsb-release (common in install instructions for CLIs)
RUN apt-get -y install git procps lsb-release
RUN apt-get install -y tzdata
RUN ln -fs /usr/share/zoneinfo/Europe/Berlin /etc/localtime
RUN dpkg-reconfigure --frontend noninteractive tzdata

RUN apt-get install -y wget
RUN apt-get update

RUN apt-get install -y software-properties-common
RUN apt-get install -y python3 python3-pip
RUN apt-get install -y git cppcheck 
RUN apt-get install -y vera++ astyle

RUN wget  https://apt.llvm.org/llvm-snapshot.gpg.key
RUN apt-key add llvm-snapshot.gpg.key
RUN add-apt-repository 'deb http://apt.llvm.org/disco/ llvm-toolchain-disco-8 main'
RUN apt-get update
RUN apt-get install -y gcc-9 g++-9 gdb
RUN apt-get install -y clang-8 clang-tools-8 libclang1-8 clang-format-8 python-clang-8 clang-tidy-8 cmake

RUN ln -s /usr/lib/llvm-8/share/clang/run-clang-tidy.py /usr/bin/run-clang-tidy.py

RUN pip3 install scikit-build cmake cmake-format
RUN pip3 install ninja gcovr lizard jinja2

ENV DEBIAN_FRONTEND=dialog

# Set the default shell to bash instead of sh
ENV SHELL /bin/bash
